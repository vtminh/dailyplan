import 'package:floor/floor.dart';

import 'Task.dart';

@dao
abstract class TaskDao {
  @Insert(onConflict : OnConflictStrategy.replace)
  Future<List<int>> insertTasks(List<Task> tasks);

  @Query('SELECT * FROM Task WHERE date = :date')
  Future<List<Task>> getAllTasks(String date);

  @Query('SELECT * FROM Task WHERE id = :id')
  Stream<Task?> findTaskById(int id);

  @Insert(onConflict : OnConflictStrategy.replace)
  Future<void> insertTask(Task task);

  @Query('DELETE FROM Task WHERE id = :id')
  Future<void> delete(int id);
}
