import 'dart:async';

import 'package:floor/floor.dart';
import 'package:flutter_complete_guide/model/CategoryDao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:flutter_complete_guide/model/Category.dart';

part 'CategoryDatabase.g.dart';


@Database(version: 1, entities: [Category])
abstract class CategoryDatabase extends FloorDatabase {
  CategoryDao get categoryDao;
}
