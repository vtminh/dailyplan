import 'package:floor/floor.dart';
import 'package:flutter_complete_guide/model/Category.dart';

@Entity(
  tableName: 'Task',
  // foreignKeys: [
  //   ForeignKey(
  //     childColumns: ['parentId'],
  //     parentColumns: ['id'],
  //     entity: Category,
  //   )
  // ],
)
class Task {
  @primaryKey
  int id = 0;
  int parentId = 0;
  String title;
  int target;
  int? complete;
  String? date;

  Task(this.id, this.title, this.target, this.parentId, {this.date, this.complete});

  String getResultString() {
    return (this.complete ?? 0).toString() + '/' + this.target.toString();
  }

  int getPercentage() {
    return (this.complete ?? 0) * 100 ~/ this.target;
  }

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(json['id'], json['title'], json['target'], json['parentId'], date: json['date']);
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'target': target,
    'parentId': parentId,
    'date': date,
  };

  bool isWarning() {
    //TODO
    return getPercentage() < 50;
  }

  bool isValid() {
    return title.isNotEmpty && target > 0;
  }
}
