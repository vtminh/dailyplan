import 'dart:convert';

import 'package:floor/floor.dart';

import 'Task.dart';

@Entity(
  tableName: 'Category',
)
class Category {
  @primaryKey
  int id = 0;
  String title;
  int percentage = 0;

  @ignore
  List<Task> tasks;

  Category(this.id, this.title, {this.tasks = const []});

  int getResult() {
    if (tasks.isEmpty) return 0;
    return tasks.map((e) => e.getPercentage()).reduce((a, b) => a + b) ~/
        tasks.length;
  }

  bool isValid() {
    return this.title.isNotEmpty;
  }

  factory Category.fromJson(Map<String, dynamic> json) {
    var tasksJson = jsonDecode(json['tasks']) as List;
    var taskObjects =
        tasksJson.map((taskJson) => Task.fromJson(taskJson)).toList();
    return Category(json['id'], json['title'], tasks: taskObjects);
  }

  Map<String, dynamic> toJson() =>
      {'id': id, 'title': title, 'tasks': jsonEncode(tasks)};
}
