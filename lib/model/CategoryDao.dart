import 'package:floor/floor.dart';
import 'package:flutter_complete_guide/model/Category.dart';

@dao
abstract class CategoryDao {
  @Insert(onConflict : OnConflictStrategy.replace)
  Future<List<int>> insertCategories(List<Category> tasks);

  @Query('SELECT * FROM Category')
  Future<List<Category>> getAllCategories();

  @Query('SELECT * FROM Category WHERE id = :id')
  Stream<Category?> findCategoryById(int id);

  @insert
  Future<void> insertCategory(Category category);

  @Query('DELETE FROM Category WHERE id = :id')
  Future<void> delete(int id);
}