import 'dart:async';
import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'Task.dart';
import 'TaskDao.dart';

part 'TaskDatabase.g.dart';

@Database(version: 1, entities: [Task])
abstract class TaskDatabase extends FloorDatabase {
  TaskDao get taskDao;
}