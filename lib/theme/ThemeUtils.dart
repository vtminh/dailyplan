import 'package:flutter/material.dart';

InputDecoration textFieldDecoration = InputDecoration(
    enabledBorder: UnderlineInputBorder(
  borderSide: BorderSide(color: Colors.black12),
));


TextStyle headerTextStyle =  TextStyle(
fontSize: 20.0,
fontWeight: FontWeight.bold,
fontStyle: FontStyle.italic);