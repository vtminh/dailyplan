import 'package:flutter_complete_guide/model/Category.dart';
import 'package:flutter_complete_guide/model/Task.dart';


const String TEMPLATE_NAME = "Template1";
List<Category> initCategories = [
  new Category(0, 'Working', tasks: [
    Task(0, '8h working', 8, 0),
    Task(1, '30m Android', 2, 0),
    Task(2, '30m Algorithm', 8, 0),
    Task(3, '30m trading', 2, 0),
  ]),
  new Category(1, 'English', tasks: [
    Task(4, '30m learn English', 8, 1),
    Task(5, 'Vocabulary in working', 2, 1),
    Task(6, 'Watch English movie', 2, 1)
  ]),
  new Category(2, 'English', tasks: [
    Task(7, '30m learn English', 8, 2),
    Task(8, 'Vocabulary in working', 2, 2),
    Task(9, 'Watch English movie', 2, 2)
  ]),
];