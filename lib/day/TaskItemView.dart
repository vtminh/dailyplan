import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/model/Task.dart';
import 'package:flutter_complete_guide/theme/ThemeUtils.dart';

class TaskItemView extends StatefulWidget {
  final Task task;
  bool editMode = false;
  int parentIndex;
  int childIndex;
  final Function() onSeekbarChanged;
  Function(int parentIndex, int childIndex, Task task)? onEdited;
  Function(int parentIndex, int childIndex)? onRemoved;

  TaskItemView(this.parentIndex, this.childIndex, this.task, this.editMode,
      this.onSeekbarChanged,
      {this.onEdited, this.onRemoved});

  @override
  _TaskItemViewState createState() => _TaskItemViewState();
}

class _TaskItemViewState extends State<TaskItemView> {
  int _currentSliderValue = 0;

  @override
  Widget build(BuildContext context) {
    developer.log('minhvt', name: '_DayItemViewState build');

    Task _tmpTask = Task(widget.task.id, widget.task.title, widget.task.target,
        widget.task.parentId,
        complete: widget.task.complete);

    double screenWidth = MediaQuery.of(context).size.width;

    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
            width: screenWidth / 3,
            child: widget.editMode
                ? TextField(
                    decoration: textFieldDecoration,
                    controller: TextEditingController(text: widget.task.title),
                    onChanged: (text) {
                      _tmpTask.title = text;
                      widget.onEdited?.call(
                          widget.parentIndex, widget.childIndex, _tmpTask);
                    },
                  )
                : Text(widget.task.title)),
        Expanded(
          child: Slider(
            value: _currentSliderValue.toDouble(),
            min: 0,
            max: widget.task.target.toDouble(),
            divisions: widget.task.target,
            label: _currentSliderValue.toString() +
                '/' +
                widget.task.target.toString(),
            onChanged: widget.editMode
                ? null
                : (double value) {
                    setState(() {
                      widget.onSeekbarChanged();
                      widget.task.complete = value.toInt();
                      _currentSliderValue = value.toInt();
                    });
                  },
          ),
        ),
        Align(
            alignment: Alignment.centerRight,
            child: SizedBox(
                width: widget.editMode ? 80 : 40,
                child: Row(children: [
                  SizedBox(
                    width: 40,
                    child: !widget.editMode
                        ? Text(widget.task.getResultString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: widget.task.isWarning()
                                    ? Colors.orangeAccent
                                    : Colors.green))
                        : TextField(
                        decoration: textFieldDecoration,
                        controller: TextEditingController(
                                text: widget.task.target.toString()),
                            onChanged: (text) {
                              if (int.tryParse(text) != null) {
                                _tmpTask.target = int.parse(text);
                                widget.onEdited?.call(widget.parentIndex,
                                    widget.childIndex, _tmpTask);
                              }
                            },
                            keyboardType: TextInputType.number),
                  ),
                  Visibility(
                    child: SizedBox(
                      width: 40,
                      child: IconButton(
                          onPressed: () {
                            widget.onRemoved
                                ?.call(widget.parentIndex, widget.childIndex);
                          },
                          icon: Icon(Icons.delete)),
                    ),
                    visible: widget.editMode,
                  ),
                ]))),
      ]),
    );
  }
}
