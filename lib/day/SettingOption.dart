enum SettingOption { EDIT, SAVE_TEMPLATE, IMPORT_TEMPLATE }

extension SettingOptionExtension on SettingOption {
  String get name {
    switch (this) {
      case SettingOption.EDIT:
        return 'Edit';
      case SettingOption.SAVE_TEMPLATE:
        return 'Save Template';

      case SettingOption.IMPORT_TEMPLATE:
        return 'Import Template';
      default:
        return '';
    }
  }
}