import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/datetime/DateTimeUtils.dart';
import 'package:flutter_complete_guide/model/Category.dart';
import 'package:flutter_complete_guide/model/CategoryDatabase.dart';
import 'package:flutter_complete_guide/model/Task.dart';
import 'package:flutter_complete_guide/model/TaskDatabase.dart';
import 'package:flutter_complete_guide/sharedprefs/SharedPrefsUtil.dart';
import 'package:flutter_complete_guide/template/InitTemplate.dart';
import 'dart:developer' as developer;

import 'AddCategoryItemView.dart';
import 'AddTaskItemView.dart';
import 'DialogHelper.dart';
import 'CategoryItemView.dart';
import 'SettingOption.dart';
import 'TaskItemView.dart';
import 'dart:collection';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class DayDetailPage extends StatefulWidget {
  String _date;
  List<Category> _categories = [];

  DayDetailPage(this._date);

  @override
  _DayDetailPageState createState() => new _DayDetailPageState();
}

class _DayDetailPageState extends State<DayDetailPage> {
  bool _editMode = false;

  void _changeMode() {
    setState(() {
      this._editMode = !this._editMode;
    });
  }

  onSeekbarChanged() {
    setState(() {});
  }

  onTaskAdded(int parentIndex, Task addedTask) async {
    this.setState(() {
      widget._categories[parentIndex].tasks.add(addedTask);
    });
    taskDatabase.taskDao.insertTask(addedTask);
  }

  onCategoryAdded(Category addedCategory) async {
    this.setState(() {
      widget._categories.add(addedCategory);
    });
    categoryDatabase.categoryDao.insertCategory(addedCategory);
  }

  onTaskEdited(int parentIndex, int childIndex, Task editedTask) async {
    setState(() {
      widget._categories[parentIndex].tasks[childIndex] = editedTask;
    });

    await this.taskDatabase.taskDao.insertTask(editedTask);
  }

  onCategoryEdited(int parentIndex, Category editedCategory) async {
    setState(() {
      widget._categories[parentIndex] = editedCategory;
    });

    await this.categoryDatabase.categoryDao.insertCategory(editedCategory);
  }

  onCategoryRemoved(int parentIndex) async {
    int removedId = widget._categories[parentIndex].id;
    setState(() {
      widget._categories.removeAt(parentIndex);
    });

    await this.taskDatabase.taskDao.delete(removedId);
  }

  onTaskRemoved(int parentIndex, int childIndex) async {
    List<Task> tasks = widget._categories[parentIndex].tasks;
    int deletedTaskId = tasks[childIndex].id;
    setState(() {
      tasks.removeAt(childIndex);
    });

    await this.taskDatabase.taskDao.delete(deletedTaskId);
  }

  late TaskDatabase taskDatabase;
  late CategoryDatabase categoryDatabase;

  @override
  void initState() {
    super.initState();
    developer.log("initState", name: widget._categories.toString());
    () async {
      SharedPreferences sp = await SharedPreferences.getInstance();
      List<String>? templates = sp.getStringList(TEMPLATE_LIST);
      if (templates == null || templates.isEmpty) {
        sp.setStringList(TEMPLATE_LIST, [TEMPLATE_NAME]);
        String value = jsonEncode(initCategories);
        await sp.setString(TEMPLATE_NAME, value);
      }
      setState(() {});
    }.call();

    $FloorTaskDatabase
        .databaseBuilder('TaskDatabase.db')
        .build()
        .then((value) async {
      this.taskDatabase = value;
    });

    $FloorCategoryDatabase
        .databaseBuilder('CategoryDatabase.db')
        .build()
        .then((value) async {
      this.categoryDatabase = value;
    });
  }

  Future<List<Category>> getAllTasks() async {

    if (widget._categories.isNotEmpty) {
      return widget._categories;
    }

    List<Category> dbCategoryList =
        await categoryDatabase.categoryDao.getAllCategories();

    List<Task> dbTaskList =
        await taskDatabase.taskDao.getAllTasks(widget._date);


    Map<int, List<Task>> taskMap = {};
    dbTaskList.forEach((task) {
      if (taskMap.containsKey(task.parentId)) {
        taskMap[task.parentId]?.add(task);
      } else {
        taskMap[task.parentId] = [];
      }
    });

    dbCategoryList.forEach((category) {
      if (taskMap[category.id] != null) {
        List<Task> listTask = List.from(category.tasks);
        listTask.addAll(taskMap[category.id]!.toList());
        category.tasks = listTask;
      }
    });

    return dbCategoryList.where((element) => element.tasks.isNotEmpty).toList();
  }

  Future<List<int>> addAllTasksToDb() async {
    List<Task> allTasks = [];
    widget._categories.forEach((category) {
      category.tasks.forEach((task) {
        task.date = DateFormat(DATE_FORMAT).format(DateTime.now());
        allTasks.add(task);
      });
    });
    return await taskDatabase.taskDao.insertTasks(allTasks);
  }

  Future<List<int>> addAllCategoriesToDb() async {
    List<Category> allCategories = [];
    allCategories.addAll(widget._categories);
    return await categoryDatabase.categoryDao.insertCategories(allCategories);
  }

  void handleClick(SettingOption value) {
    switch (value) {
      case SettingOption.EDIT:
        setState(() {
          _editMode = true;
        });
        break;
      case SettingOption.SAVE_TEMPLATE:
        new Future.delayed(Duration.zero, () {
          showConfirmAddTemplateDialog(
              context, (name) => {saveTemplate(name, widget._categories)});
        });

        break;

      case SettingOption.IMPORT_TEMPLATE:
        openImportTemplateDialog();
        break;
    }
  }

  void openImportTemplateDialog() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    List<String>? templates = sp.getStringList(TEMPLATE_LIST);

    if (templates?.isNotEmpty == true) {
      showImportTemplateDialog(context, templates!, (value) {
        if (sp.getString(value)?.isNotEmpty == true) {
          var json = jsonDecode(sp.getString(value)!) as List;
          setState(() {
            widget._categories =
                json.map((tagJson) => Category.fromJson(tagJson)).toList();
          });

          () async {
            await addAllCategoriesToDb();
            await addAllTasksToDb();
          }.call();
        }
      });
    }
  }

  void saveTemplate(String name, List<Category> categories) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? templates = prefs.getStringList(TEMPLATE_LIST);
    String value = jsonEncode(categories);
    await prefs.setString(name, value);
    if (templates != null && templates.isNotEmpty) {
      templates.add(name);
      await prefs.setStringList(TEMPLATE_LIST, templates);
    } else {
      await prefs.setStringList(TEMPLATE_LIST, [name]);
    }
  }

  @override
  Widget build(BuildContext context) {
    developer.log('minhvt', name: 'build');
    return new Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("Day Plan"),
        actions: <Widget>[
          _editMode
              ? IconButton(
                  icon: Icon(Icons.check),
                  onPressed: () {
                    _changeMode();
                  },
                )
              : PopupMenuButton<SettingOption>(
                  onSelected: handleClick,
                  itemBuilder: (BuildContext context) {
                    return SettingOption.values.map((SettingOption choice) {
                      return PopupMenuItem<SettingOption>(
                        value: choice,
                        child: Text(choice.name),
                      );
                    }).toList();
                  },
                ),
        ],
      ),
      body: FutureBuilder(
          future: this.getAllTasks(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Category>> snapshot) {
            if (snapshot.hasData && snapshot.data!.isNotEmpty) {
              widget._categories = snapshot.data!;
              return new ListView.builder(
                itemCount: _editMode
                    ? widget._categories.length + 1
                    : widget._categories.length,
                itemBuilder: (context, i) {
                  return (_editMode && i == widget._categories.length)
                      ? AddCategoryItemView(() {
                          showCategoryAddDialog(context, (category) {
                            onCategoryAdded(category);
                          });
                        })
                      : ExpansionTile(
                          title: CategoryItemView(
                            i,
                            widget._categories[i],
                            _editMode,
                            onRemoved: onCategoryRemoved,
                            onEdited: onCategoryEdited,
                          ),
                          children: <Widget>[
                            new Column(
                                children: _createTaskItemView(
                              widget._categories[i].tasks,
                              i,
                              widget._categories[i],
                            )),
                          ],
                        );
                },
              );
            } else {
              return Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('There are no records of this day.'),
                  InkWell(
                      child: new Text(
                        'Import Template',
                        style: TextStyle(color: Colors.cyan),
                      ),
                      onTap: () => openImportTemplateDialog())
                ],
              ));
            }
          }),
    );
  }

  List<StatefulWidget> _createTaskItemView(
      List<Task> tasks, int parentIndex, Category category) {
    List<StatefulWidget> items = [];
    items.addAll(tasks
        .asMap()
        .map((index, element) => MapEntry(
            index,
            TaskItemView(
              parentIndex,
              index,
              element,
              _editMode,
              onSeekbarChanged,
              onEdited: onTaskEdited,
              onRemoved: onTaskRemoved,
            )))
        .values
        .toList());

    items.add(new AddTaskItemView(parentIndex, _editMode, () {
      showTaskAddDialog(context, category, (task) {
        onTaskAdded(parentIndex, task);
      });
    }));

    return items;
  }
}
