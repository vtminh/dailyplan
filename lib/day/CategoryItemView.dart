import 'package:flutter/cupertino.dart';
import 'dart:developer' as developer;
import 'package:flutter/material.dart';

import 'package:flutter_complete_guide/model/Category.dart';
import 'package:flutter_complete_guide/theme/ThemeUtils.dart';

import 'DialogHelper.dart';

class CategoryItemView extends StatefulWidget {
  final Category category;
  bool editMode = false;
  int parentIndex;

  Function(int parentIndex, Category category)? onEdited;
  Function(int parentIndex)? onRemoved;

  CategoryItemView(this.parentIndex, this.category, this.editMode,
      {this.onEdited, this.onRemoved});

  @override
  _CategoryItemViewState createState() => _CategoryItemViewState();
}

class _CategoryItemViewState extends State<CategoryItemView> {
  @override
  Widget build(BuildContext context) {
    developer.log('minhvt', name: '_DayItemViewState build');

    Category _tmpCategory = Category(widget.category.id, widget.category.title,
        tasks: widget.category.tasks);
    _tmpCategory.percentage = widget.category.percentage;

    double screenWidth = MediaQuery
        .of(context)
        .size
        .width;

    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Visibility(
        child: SizedBox(
          width: 40,
          child: IconButton(
              onPressed: () {
                showConfirmRemoveDialog(context, widget.category, () => {
                widget.onRemoved?.call(widget.parentIndex)
                });
              },
              icon: Icon(Icons.delete)),
        ),
        visible: widget.editMode,
      ),
      Expanded(
          child: widget.editMode
              ? Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0),
              child: TextField(
                decoration: textFieldDecoration,
                controller:
                TextEditingController(text: widget.category.title),
                style: new TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
                onChanged: (text) {
                  _tmpCategory.title = text;
                  widget.onEdited?.call(widget.parentIndex, _tmpCategory);
                },
              ))
              : Text(
            widget.category.title +
                ' - ' +
                widget.category.getResult().toString() +
                '%',
            style: headerTextStyle,
          )),
    ]);
  }
}
