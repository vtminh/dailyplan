import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/model/Category.dart';
import 'package:flutter_complete_guide/model/Task.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

void showTaskAddDialog(
    BuildContext context, Category category, Function(Task) callback) {
  Task task =
      new Task(DateTime.now().millisecondsSinceEpoch, '', 0, category.id);

  AlertDialog addDialog = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return Container(
          width: 300.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: 30.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Text(
                  'Add more task for ${category.title} ?',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  decoration: InputDecoration(labelText: 'Name'),
                  onChanged: (text) {
                    task.title = text;
                    setState.call(() => {});
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  decoration: InputDecoration(labelText: 'Score'),
                  onChanged: (text) {
                    if (int.tryParse(text) != null) {
                      task.target = int.parse(text);
                    }
                    setState.call(() => {});
                  },
                  keyboardType: TextInputType.number,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Container(
                    width: double.infinity,
                    height: 60,
                    child: TextButton(
                        onPressed: task.isValid()
                            ? () {
                                callback.call(task);
                                Navigator.of(context).pop();
                              }
                            : null,
                        child: Text('ADD',
                            style: TextStyle(fontWeight: FontWeight.bold)))),
              ),
            ],
          ),
        );
      }));

  showDialog(context: context, builder: (BuildContext context) => addDialog);
}

void showCategoryAddDialog(BuildContext context, Function(Category) callback) {
  Category category = new Category(DateTime.now().millisecondsSinceEpoch, '');

  AlertDialog addDialog = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return Container(
          width: 300.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: 30.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Text(
                  'Add new category for your plan.',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  decoration: InputDecoration(labelText: 'Category Name'),
                  onChanged: (text) {
                    category.title = text;
                    setState.call(() => {});
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Container(
                    width: double.infinity,
                    height: 60,
                    child: TextButton(
                        onPressed: category.isValid()
                            ? () {
                                callback.call(category);
                                Navigator.of(context).pop();
                              }
                            : null,
                        child: Text('ADD',
                            style: TextStyle(fontWeight: FontWeight.bold)))),
              ),
            ],
          ),
        );
      }));

  showDialog(context: context, builder: (BuildContext context) => addDialog);
}

void showConfirmRemoveDialog(
    BuildContext context, Category category, Function() callback) {
  AlertDialog addDialog = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return Container(
          width: 300.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: 30.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Text(
                  'Do you want to remove ${category.title} ? All of tasks of this category will be removed as well.',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('CANCEL',
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold))),
                    TextButton(
                        onPressed: () {
                          callback.call();
                          Navigator.of(context).pop();
                        },
                        child: Text('REMOVE',
                            style: TextStyle(fontWeight: FontWeight.bold)))
                  ],
                ),
              ),
            ],
          ),
        );
      }));

  showDialog(context: context, builder: (BuildContext context) => addDialog);
}

void showConfirmAddTemplateDialog(
    BuildContext context, Function(String) callback) {
  String name = '';
  AlertDialog addDialog = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return Container(
          width: 300.0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                    top: 30.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: Text(
                  'Do you want to save this plan to your template ?',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  decoration: InputDecoration(labelText: 'Template Name'),
                  onChanged: (text) {
                    name = text;
                    setState.call(() => {});
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text('CANCEL',
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold))),
                    TextButton(
                        onPressed: name.isNotEmpty
                            ? () {
                                () async {
                                  SharedPreferences sp =
                                      await SharedPreferences.getInstance();
                                  String? result = sp.getString(name);
                                  if (result != null && result.isNotEmpty) {
                                    Fluttertoast.showToast(
                                      msg:
                                          "This name is already existed. Please choose another name.",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                    );
                                  } else {
                                    callback.call(name);
                                    Navigator.of(context).pop();
                                  }
                                }.call();
                              }
                            : null,
                        child: Text('ADD',
                            style: TextStyle(fontWeight: FontWeight.bold)))
                  ],
                ),
              ),
            ],
          ),
        );
      }));

  showDialog(context: context, builder: (BuildContext context) => addDialog);
}

void showImportTemplateDialog(
    BuildContext context, List<String> options, Function(String) callback) {
  AlertDialog addDialog = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return Container(
            width: 300.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      top: 10.0, bottom: 20.0, left: 10.0, right: 10.0),
                  child: Text(
                    'Import Template:',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: options.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                          child: CheckboxListTile(
                            title: Text(options[index]),
                            value: false,
                            onChanged: (bool? value) {
                              setState.call(() => {});
                              callback.call(options[index]);
                              Navigator.of(context).pop();
                            },
                          ),
                          // onTap: () {
                          //
                          // }
                          );
                    })
              ],
            ));
      }));

  showDialog(context: context, builder: (BuildContext context) => addDialog);
}
