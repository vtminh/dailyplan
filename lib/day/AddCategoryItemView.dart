import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/theme/ThemeUtils.dart';

class AddCategoryItemView extends StatefulWidget {
  final Function() onClicked;

  AddCategoryItemView(this.onClicked);

  @override
  _AddCategoryItemViewState createState() => _AddCategoryItemViewState();
}

class _AddCategoryItemViewState extends State<AddCategoryItemView> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onClicked.call(),
      child: Padding(
          padding:
              EdgeInsets.only(left: 24.0, right: 24.0, top: 16.0, bottom: 16.0),
          child: Row(
            children: [
              Icon(Icons.add),
              Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Text(
                    "Add more Category",
                    style: headerTextStyle,
                  ))
            ],
          )),
    );
  }
}
