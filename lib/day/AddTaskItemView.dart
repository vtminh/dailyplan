import 'package:flutter/material.dart';

class AddTaskItemView extends StatefulWidget {
  bool editMode = false;
  int parentIndex;

  final Function() onClicked;

  AddTaskItemView(this.parentIndex, this.editMode, this.onClicked);

  @override
  _AddTaskItemViewState createState() => _AddTaskItemViewState();
}

class _AddTaskItemViewState extends State<AddTaskItemView> {
  @override
  Widget build(BuildContext context) {
    return Visibility(
        child: GestureDetector(
          onTap: () => widget.onClicked.call(),
          child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                children: [Icon(Icons.add), Text("Add more Task")],
              )),
        ),
        visible: widget.editMode);
  }
}
