import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/datetime/DateTimeUtils.dart';
import 'package:flutter_complete_guide/day/DayDetailPage.dart';
import 'DayInfo.dart';
import 'package:intl/intl.dart';
import 'dart:developer' as developer;

class WeekList extends StatefulWidget {
  @override
  _WeekListState createState() => _WeekListState();
}

class _WeekListState extends State<WeekList> {
  final List<String> days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

  var timestamp =
      DateFormat(DATE_FORMAT).parse("30/08/2021").millisecondsSinceEpoch;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    developer.log("minhvt",
        name: DateFormat(DATE_FORMAT).format(
            DateTime.fromMillisecondsSinceEpoch(timestamp + ONE_DAY * 0)));

    return ListView.builder(
        itemCount: days.length,
        itemBuilder: (context, index) {
          return GestureDetector(
              child: DailyInfo(days[index], 50),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new DayDetailPage(
                          DateFormat(DATE_FORMAT).format(
                              DateTime.fromMillisecondsSinceEpoch(
                                  timestamp + ONE_DAY * index)))),
                );
              });
        });
  }
}
