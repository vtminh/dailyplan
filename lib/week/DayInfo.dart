import 'package:flutter/material.dart';

class DailyInfo extends StatefulWidget {
  final String name;
  final int percentage;

  DailyInfo(this.name, this.percentage);

  @override
  _DailyInfoState createState() => _DailyInfoState(name, percentage);
}

class _DailyInfoState extends State<DailyInfo> {
  final String name;
  final int percentage;

  _DailyInfoState(this.name, this.percentage);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 5,
        margin: EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 5,
        ),
        child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(this.name),
                  Text(this.percentage.toString() + "%")
                ])));
  }
}
